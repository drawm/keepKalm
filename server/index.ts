import * as debug from 'debug'
debug.enable('kalm');

console.log('Start server');

import * as Kalm from 'kalm';
import * as ws from 'kalm-websocket';

Kalm.adapters.register('ws', ws);

const kalmServer = new Kalm.Server({
    port : 4000,
    adapter : 'ws',
    encoder : 'json',
    channels : {
        messageEvent : (data) => {
            // Handler - new connections will register to these events
            console.log('User sent message ' + data.body);
        }
    }
});

// When a connection is received, send a message to all connected u    sers
kalmServer.on('connection', (client) => {    // Handler, where client i    s an instance of Kalm.Client
    console.log("connection");
    kalmServer.broadcast('userEvent', 'A new user has connected');
});

kalmServer.subscribe('/hello', (data) => {
    console.log(data);  // 'Hello from Client'
});


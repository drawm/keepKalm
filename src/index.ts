import * as debug from 'debug';
debug.enable("kalm");

import Kalm from 'kalm';
import ws from 'kalm-websocket';

Kalm.adapters.register('ws', ws);

// Opens a connection to the server
let client = new Kalm.Client({
    hostname: '0.0.0.0', // Server's IP
    port: 4000, // Server's port
    adapter: 'ws', // Server's adapter
    encoder: 'json', // Server's encoder
});

client.send('/hello', 'world');
client.subscribe('userEvent', console.log);

console.log('helloworld')

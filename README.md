# Usage

#### Start client
```bash
npm run start
```

#### Start server
```bash
npm run server.start
```